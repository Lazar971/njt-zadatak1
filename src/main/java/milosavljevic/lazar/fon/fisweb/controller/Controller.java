/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package milosavljevic.lazar.fon.fisweb.controller;

import javax.servlet.http.HttpServletRequest;
import milosavljevic.lazar.fon.fisweb.action.AbstractAction;
import milosavljevic.lazar.fon.fisweb.action.factory.ActionFactory;
import milosavljevic.lazar.fon.fisweb.constants.PageConstants;


/**
 *
 * @author Lazar
 */
public class Controller {

    public String processRequest(String pathInfo, HttpServletRequest req) {
        String nextPage = PageConstants.VIEW_DEFAULT_ERROR;
        
        AbstractAction action = ActionFactory.createActionFactory(pathInfo);
        if (action != null) {
            nextPage = action.execute(req);
        }
        
        return nextPage;
    }

    
    
    
}
