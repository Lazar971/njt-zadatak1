/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package milosavljevic.lazar.fon.fisweb.frontcontroller;

import milosavljevic.lazar.fon.fisweb.controller.Controller;
import milosavljevic.lazar.fon.fisweb.viewresolver.ViewResolver;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Lazar
 */
@WebServlet("/app/*")
public class FrontController  extends HttpServlet{

    private Controller controller;
    private ViewResolver viewResolver;
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
         this.handleRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
      this.handleRequest(req, resp);
    }
    
    @Override
    public void init() throws ServletException {
        this.controller=new Controller();
        this.viewResolver=new ViewResolver();
    }
    
    private void handleRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
        resp.setContentType("text/html;charset=UTF-8");
        resp.setHeader("Cache-Control", "private, no-store, no-cache, must-revalidate");
        String view = controller.processRequest(req.getPathInfo(), req);
        
        String page = viewResolver.getPage(view);
        
         req.getRequestDispatcher(page).forward(req, resp);
    }
    
}
