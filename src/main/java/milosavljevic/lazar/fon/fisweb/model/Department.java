/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package milosavljevic.lazar.fon.fisweb.model;

/**
 *
 * @author Lazar
 */
public class Department {
    
    private int id;
    private String name;
    private String shortname;
    private static int idGen=1;
    public Department() {
    }

    public Department( String name, String shortname) {
        this.id = idGen++;
        this.name = name;
        this.shortname = shortname;
    }

    public String getShortname() {
        return shortname;
    }

    public void setShortname(String shortname) {
        this.shortname = shortname;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    
}
