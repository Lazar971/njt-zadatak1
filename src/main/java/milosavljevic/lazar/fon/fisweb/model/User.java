/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package milosavljevic.lazar.fon.fisweb.model;

/**
 *
 * @author Lazar
 */
public class User {
    private String ime;
    private String prezime;
    private String email;
    private String password;

    public User(String ime, String prezime, String email, String password) {
        this.ime = ime;
        this.prezime = prezime;
        this.email = email;
        this.password = password;
    }

    public User() {
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    
}
