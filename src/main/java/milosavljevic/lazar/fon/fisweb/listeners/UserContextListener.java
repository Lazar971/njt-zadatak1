/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package milosavljevic.lazar.fon.fisweb.listeners;

import milosavljevic.lazar.fon.fisweb.model.User;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 *
 * @author Lazar
 */
@WebListener
public class UserContextListener implements ServletContextListener{

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        
        sce.getServletContext().setAttribute("users", createUsers());
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        sce.getServletContext().setAttribute("users", null);
    }
    private List<User> createUsers(){
        List<User> users=new ArrayList<>();
        users.add(new User("ime1", "prezime1", "ip1@gmail.com", "sifra1"));
        users.add(new User("ime2", "prezime2", "ip2@gmail.com", "sifra1"));
        users.add(new User("ime3", "prezime3", "ip3@gmail.com", "sifra1"));
        users.add(new User("ime4", "prezime4", "ip4@gmail.com", "sifra1"));
        users.add(new User("ime5", "prezime5", "ip5@gmail.com", "sifra1"));
        return users;
    }
}
