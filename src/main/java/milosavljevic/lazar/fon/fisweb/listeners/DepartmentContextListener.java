/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package milosavljevic.lazar.fon.fisweb.listeners;

import milosavljevic.lazar.fon.fisweb.model.Department;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 *
 * @author Lazar
 */
@WebListener
public class DepartmentContextListener implements ServletContextListener{

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        sce.getServletContext().setAttribute("departments", createDepartments());
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        sce.getServletContext().setAttribute("departments", null);
    }
    private List<Department> createDepartments(){
        List<Department> departments=new ArrayList<>();
        departments.add(new Department("department1", "dep1"));
        departments.add(new Department("department2", "dep2"));
        departments.add(new Department("department3", "dep3"));
        departments.add(new Department("department4", "dep4"));
        
        return departments;
    }
}
