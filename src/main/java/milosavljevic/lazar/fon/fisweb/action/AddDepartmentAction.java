/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package milosavljevic.lazar.fon.fisweb.action;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import milosavljevic.lazar.fon.fisweb.constants.PageConstants;
import milosavljevic.lazar.fon.fisweb.model.Department;

/**
 *
 * @author Lazar
 */
public class AddDepartmentAction extends AbstractAction {

    @Override
    public String execute(HttpServletRequest req) {
        String name=(String)req.getParameter("name");
        String shortname=(String)req.getParameter("shortname");
        Department d=new Department(name, shortname);
        List<Department> departments=(List<Department>) req.getServletContext().getAttribute("departments");
        departments.add(d);
        return PageConstants.View_Add_Departments;
    }
    
}
