/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package milosavljevic.lazar.fon.fisweb.action;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import milosavljevic.lazar.fon.fisweb.constants.PageConstants;
import milosavljevic.lazar.fon.fisweb.model.User;

/**
 *
 * @author Lazar
 */
public class LoginAction extends AbstractAction {

    @Override
    public String execute(HttpServletRequest req) {
        System.out.println("Login");
        if(req.getSession(true).getAttribute("currentUser")!=null)
            return PageConstants.View_Home;
        String email=req.getParameter("email");
        String password=req.getParameter("password");
        List<User> users=(List<User>)req.getServletContext().getAttribute("users");
        for (User user : users) {
            if(user.getEmail().equals(email) && user.getPassword().equals(password)){
                req.setAttribute("message", null);
                req.getSession(true).setAttribute("currentUser", user);
                return PageConstants.View_Home;
            }
        }
        req.setAttribute("message", "wrong");
        return PageConstants.View_Login;
    }
    
}
