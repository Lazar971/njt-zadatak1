/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package milosavljevic.lazar.fon.fisweb.action.factory;

import milosavljevic.lazar.fon.fisweb.action.AbstractAction;
import milosavljevic.lazar.fon.fisweb.action.AddDepartmentAction;
import milosavljevic.lazar.fon.fisweb.action.AddDepartmentFormAction;
import milosavljevic.lazar.fon.fisweb.action.AllDepartmentsAction;
import milosavljevic.lazar.fon.fisweb.action.DeleteDepartmentAction;
import milosavljevic.lazar.fon.fisweb.action.ErroAction;
import milosavljevic.lazar.fon.fisweb.action.LoginAction;
import milosavljevic.lazar.fon.fisweb.action.LogoutAction;
import milosavljevic.lazar.fon.fisweb.action.UpdateDepartmentAction;
import milosavljevic.lazar.fon.fisweb.action.UpdateDepartmentFormAction;
import milosavljevic.lazar.fon.fisweb.constants.ActionConstants;

/**
 *
 * @author Lazar
 */
public class ActionFactory {

    public static AbstractAction createActionFactory(String pathInfo) {
        
        if(pathInfo.equals(ActionConstants.Login))
            return new LoginAction();
        if(pathInfo.equals(ActionConstants.ALL_DEPARTMENTS))
            return new AllDepartmentsAction();
        if(pathInfo.equals(ActionConstants.LOGOUT))
            return new LogoutAction();
        if(pathInfo.equals(ActionConstants.ADD_DEPARTMENT_FORM))
            return new AddDepartmentFormAction();
        if(pathInfo.equals(ActionConstants.ADD_DEPARTMENT))
            return new AddDepartmentAction();
        if(pathInfo.equals(ActionConstants.UDPDATE_DEPARTMENT))
            return new UpdateDepartmentAction();
        if(pathInfo.startsWith(ActionConstants.DELETE_DEPARTMENT))
            return new DeleteDepartmentAction();
         if(pathInfo.startsWith(ActionConstants.UPDATE_DEPARTMENT_FORM))       
             return new UpdateDepartmentFormAction();
        return new ErroAction();
    }
    
}
