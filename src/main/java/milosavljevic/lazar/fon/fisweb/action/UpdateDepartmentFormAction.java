/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package milosavljevic.lazar.fon.fisweb.action;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import milosavljevic.lazar.fon.fisweb.constants.PageConstants;
import milosavljevic.lazar.fon.fisweb.model.Department;

/**
 *
 * @author Lazar
 */
public class UpdateDepartmentFormAction extends AbstractAction {

    @Override
    public String execute(HttpServletRequest req) {
        int id=Integer.parseInt(req.getParameter("id"));
         Department d=null;
        List<Department> departments=(List<Department>) req.getServletContext().getAttribute("departments");
        for (Department department : departments) {
            if(department.getId()==id){
                d=department;
                break;
            }
        }
        req.setAttribute("department", d);
        return PageConstants.View_Add_Departments;
    }
    
}
