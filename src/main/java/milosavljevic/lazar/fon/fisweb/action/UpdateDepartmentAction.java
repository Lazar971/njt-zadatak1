/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package milosavljevic.lazar.fon.fisweb.action;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import milosavljevic.lazar.fon.fisweb.constants.PageConstants;
import milosavljevic.lazar.fon.fisweb.model.Department;

/**
 *
 * @author Lazar
 */
public class UpdateDepartmentAction  extends AbstractAction{

    @Override
    public String execute(HttpServletRequest req) {
        System.out.println(req);
        int id=Integer.parseInt(req.getParameter("id"));
        String name=(String)req.getParameter("name");
        String shortname=(String)req.getParameter("shortname");
        System.out.println("Name: "+name);
        List<Department> departments=(List<Department>) req.getServletContext().getAttribute("departments");
        System.out.println("Departments size: "+departments.size());
        for (Department department : departments) {
            if(department.getId()==id){
                department.setName(name);
                department.setShortname(shortname);
                break;
            }
        }
        return PageConstants.View_All_Departments;
    }
    
}
