/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package milosavljevic.lazar.fon.fisweb.action;

import javax.servlet.http.HttpServletRequest;
import milosavljevic.lazar.fon.fisweb.constants.PageConstants;

/**
 *
 * @author Lazar
 */
public class LogoutAction  extends AbstractAction{

    @Override
    public String execute(HttpServletRequest req) {
        req.getSession().setAttribute("currentUser", null);
        return PageConstants.View_Login;
    }
    
}
