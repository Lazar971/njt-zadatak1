/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package milosavljevic.lazar.fon.fisweb.constants;

/**
 *
 * @author Lazar
 */
public class ActionConstants {

    public static final String Login="/login";
    public static final String ALL_DEPARTMENTS="/department/all";
    public static final String LOGOUT="/logout";
    public static final String ADD_DEPARTMENT_FORM="/department/add";
    public static final String ADD_DEPARTMENT="/add_department";
    public static final String DELETE_DEPARTMENT="/delete_department";
    public static final String UPDATE_DEPARTMENT_FORM="/edit_department";
    public static final String UDPDATE_DEPARTMENT="/update_department";
}
