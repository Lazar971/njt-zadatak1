/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package milosavljevic.lazar.fon.fisweb.constants;

/**
 *
 * @author Lazar
 */
public class PageConstants {

    public static final String VIEW_DEFAULT_ERROR="error";
    public static final String View_Login="login";
    public static final  String View_Home="home";
    public static final String View_All_Departments="allDepartments";
    public static final String View_Add_Departments="add_department";
     
    public static final String PAGE_DEFAULT_ERROR="/WEB-INF/pages/error/error.jsp";
    
    public static final String PAGE_HOME="/WEB-INF/pages/home.jsp";
    public static final String PAGE_LOGIN="/login.jsp";
    public static final String PAGE_ALL_DEPARTMENTS="/WEB-INF/pages/department/all.jsp";
    public static final String PAGE_ADD_DEPARTMENT="/WEB-INF/pages/department/add.jsp";
    
}
