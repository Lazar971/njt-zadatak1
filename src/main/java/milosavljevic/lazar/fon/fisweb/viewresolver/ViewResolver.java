/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package milosavljevic.lazar.fon.fisweb.viewresolver;

import java.util.HashMap;
import java.util.Map;
import milosavljevic.lazar.fon.fisweb.constants.PageConstants;

/**
 *
 * @author Lazar
 */
public class ViewResolver {
    private final Map<String, String> viewPageMap;

    public ViewResolver() {
        this.viewPageMap = new HashMap<>();
        
         this.viewPageMap.put(PageConstants.View_Home,PageConstants.PAGE_HOME );
         
         this.viewPageMap.put(PageConstants.VIEW_DEFAULT_ERROR, PageConstants.PAGE_DEFAULT_ERROR);
         
         this.viewPageMap.put(PageConstants.View_Login,PageConstants.PAGE_LOGIN );
         this.viewPageMap.put(PageConstants.View_All_Departments,PageConstants.PAGE_ALL_DEPARTMENTS);
         this.viewPageMap.put(PageConstants.View_Add_Departments, PageConstants.PAGE_ADD_DEPARTMENT);
    }
    public String getPage(String view){
        return viewPageMap.get(view);
    }
    
}
