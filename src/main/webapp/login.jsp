<%-- 
    Document   : login
    Created on : 21.04.2020., 00.29.14
    Author     : Lazar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <title>Login</title>
    </head>
    
    <body>
        <h1>This is login page!</h1>
        ${message}
        
            
            
                <form class="mt-5" action="/laza/app/login" method="post">
                    <div class="form-group">
                        <label  for="email">Email</label>
                        <input class="form-control" type="text" id="email" name="email"/>
                    </div >
                    
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input class="form-control" type="password" id="password" name="password"/>
                    </div >
                    <div>
                        <input class="btn btn-primary" type="submit"  value="Log in"/>
                    </div>
                </form>

         
    </body>
</html>
