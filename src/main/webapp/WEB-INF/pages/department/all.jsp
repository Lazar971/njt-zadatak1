<%-- 
    Document   : all.jsp
    Created on : 21.04.2020., 22.56.34
    Author     : Lazar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <title>All departments</title>
    </head>
    <body>
         <%@include file="../template/header.jsp" %>
        <table class="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>name</th>
                    <th>shortname</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="department" items="${applicationScope.departments}">
                    <tr>
                        <td>${department.id}</td>
                        <td>${department.name}</td>
                        <td>${department.shortname}</td>
                        <td>
                            <a href="/laza/app/delete_department?id=${department.id}">Delete</a>
                        </td>
                        <td>
                             <a href="/laza/app/edit_department?id=${department.id}">Edit</a>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
            
        </table>
    </body>
</html>
