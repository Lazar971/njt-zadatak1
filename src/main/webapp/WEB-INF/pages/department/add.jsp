<%-- 
    Document   : add
    Created on : 21.04.2020., 23.54.51
    Author     : Lazar
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Add a department</title>
    </head>
    <body>
         <%@include file="../template/header.jsp" %>
         <c:choose>
        <c:when test="${requestScope.department == null}">
            <form action="/laza/app/add_department" method="POST">
        </c:when>
        <c:otherwise>
         <form action="/laza/app/update_department" method="POST">
             <div  class="form-group">
                        <label  for="identifikator">ID</label>
                        <input class="form-control" readonly="true"  type="text" id="identifikator"  value="${requestScope.department.id}" name="id"/>
                    </div >
        </c:otherwise>
        </c:choose>
                    
                    <div class="form-group">
                        <label  for="name">Name</label>
                        <input class="form-control" type="text" id="name" name="name" value="${requestScope.department.name}"/>
                    </div >
                    
                    <div class="form-group">
                        <label for="password">Shortname</label>
                        <input class="form-control" type="text" id="shortname" name="shortname" value="${requestScope.department.shortname}"/>
                    </div >
                    <div >
                        <c:choose>
                            <c:when test="${requestScope.department != null}">
                                <input class="btn btn-primary" type="submit" value="Update " />
                                
                            </c:when>
                            <c:otherwise>
                                <input class="btn btn-primary" type="submit" value="Add " />
                            </c:otherwise>
                        </c:choose>
                        
                        
                    </div>
                </form>
    </body>
</html>
