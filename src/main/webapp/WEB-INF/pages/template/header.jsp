<%-- 
    Document   : header
    Created on : 21.04.2020., 22.32.21
    Author     : Lazar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <title>JSP Page</title>
    </head>
    <body>
       <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="/laza/app/department/all">Sve katedre</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/laza/app/department/add">Dodaj</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/laza/app/logout">Odjava</a>
                </li>

            </ul>
        </div>
    </nav>
    </body>
</html>
