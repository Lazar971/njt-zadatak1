<%-- 
    Document   : error
    Created on : 21.04.2020., 22.48.50
    Author     : Lazar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <title>Error Page</title>
    </head>
    <body>
        <h1>${error}</h1>
    </body>
</html>
