<%-- 
    Document   : home
    Created on : 21.04.2020., 22.23.08
    Author     : Lazar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <title>JSP Page</title>
    </head>
    <body>
         <%@include file="template/header.jsp" %>
        <h1>Hello World!</h1>
        <h1>${currentUser.email}</h1>
    </body>
</html>
